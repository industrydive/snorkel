import React from 'react';

import Alert from '../src/components/Alert';

export default {
  component: Alert,
  title: 'Alert',
};

const TestLink = () => (
  <>
    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
    <a href="#">
      <strong>BREAKING:</strong>
      FERC&apos;s LaFleur to step down
    </a>
  </>
);

export const Loud = () => (
  <Alert type="loud">
    <TestLink />
  </Alert>
);

export const Medium = () => (
  <Alert>
    <TestLink />
  </Alert>
);
