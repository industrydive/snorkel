import React from 'react';

import CallOutLink from '../src/components/CallOutLink';

import '../src/scss/stories/stories.scss';

export default {
  component: CallOutLink,
  title: 'CallOutLink',
};

export const CallOut = () => (
  <CallOutLink href="https://industrydive.com">Learn more </CallOutLink>
);
