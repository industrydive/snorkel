import React from 'react';

import HelpText from '../src/components/HelpText';

import '../src/scss/stories/stories.scss';

export default {
  component: HelpText,
  title: 'HelpText',
};

export const Help = () => (
  <HelpText>
    By signing up you agree to our
    {' '}
    <a href="http://www.industrydive.com/privacy-policy/" target="_blank" rel="noopener noreferrer">privacy policy</a>
    . You can opt out anytime.
  </HelpText>
);

Help.story = {
  name: 'Help Text',
};

export const SmallHelpText = () => (
  <HelpText size="small">
    By signing up you agree to our
    {' '}
    <a href="http://www.industrydive.com/privacy-policy/" target="_blank" rel="noopener noreferrer">privacy policy</a>
    . You can opt out anytime.
  </HelpText>
);
