import React from 'react';

import PostLabel from '../src/components/PostLabel';

import '../src/scss/stories/stories.scss';

export default {
  component: PostLabel,
  title: 'PostLabel',
};

export const LoudPostLabel = () => (
  <PostLabel type="loud">Brief</PostLabel>
);

export const MediumPostLabel = () => (
  <PostLabel type="medium">Opinion</PostLabel>
);

export const SponsoredPostLabel = () => (
  <PostLabel type="sponsored">Sponsored</PostLabel>
);

export const PostLabelWithIcon = () => (
  <PostLabel type="loud">
    <PostLabel.Icon
      alt="podcast icon"
      src="https://www.utilitydive.com//static/img/components/labels/podcasts/podcast_white.svg"
    />
    Podcast
  </PostLabel>
);
