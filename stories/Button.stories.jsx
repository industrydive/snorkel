import React from 'react';

import Button from '../src/components/Button';

export default {
  component: Button,
  title: 'Button',
};

export const Loud = () => (
  <Button buttonType="loud">
    Learn more
  </Button>
);

export const Medium = () => (
  <Button buttonType="medium">
    Register
  </Button>
);

export const Soft = () => (
  <Button buttonType="soft">
    Download
  </Button>
);

export const FullWidth = () => (
  <Button fullWidth>
    Sign up
  </Button>
);

export const Disabled = () => (
  <Button disabled>
    Download
  </Button>
);

export const LinkButton = () => (
  <Button as="a" href="https://industrydive.com">
    Click Here
  </Button>
);

export const RightIcon = () => (
  <Button>
    Next
    <Button.Icon
      alt="Next"
      direction="right"
      src="https://www.fooddive.com/static/img/components/buttons/right-arrow.svg"
    />
  </Button>
);

export const LeftIcon = () => (
  <Button>
    <Button.Icon
      alt="Previous"
      direction="left"
      src="https://www.fooddive.com/static/img/components/buttons/left-arrow.svg"
    />
    Previous
  </Button>
);

export const SmallIcon = () => (
  <Button>
    Next
    <Button.Icon
      alt="Next"
      direction="right"
      size="small"
      src="https://www.fooddive.com/static/img/components/buttons/right-arrow.svg"
    />
  </Button>
);
