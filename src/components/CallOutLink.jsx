import React from 'react';
import PropTypes from 'prop-types';

import '../scss/components/CallOutLink.scss';

const CallOutLink = (props) => {
  const {
    children,
    href,
  } = props;

  return (
    <a href={href} className="call-out-link">
      {children}
      <span className="call-out-link__arrow">➔</span>
    </a>
  );
};

CallOutLink.propTypes = {
  children: PropTypes.element.isRequired,
  href: PropTypes.string.isRequired,
};

export default CallOutLink;
