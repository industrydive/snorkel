import cx from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';

import '../scss/components/Alert.scss';

const Alert = (props) => {
  const {
    type,
    children,
  } = props;

  const wrapperClasses = cx({
    'site-alert': true,
    'site-alert--loud': type === 'loud',
  });

  const messageClasses = cx({
    'site-alert__message': true,
    'site-alert__message--inverse': type === 'loud',
  });

  const imageColor = type === 'loud' ? 'white' : 'black';

  return (
    <div className={wrapperClasses}>
      <div className="row">
        <div className="columns">
          <div className={messageClasses}>
            {children}
            <div className="site-alert__close">
              <img src={`https://utilitydive.com/static/img/close-${imageColor}.svg`} alt="close" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Alert.propTypes = {
  type: PropTypes.string,
  children: PropTypes.node.isRequired,
};

Alert.defaultProps = {
  type: 'medium',
};

export default Alert;
