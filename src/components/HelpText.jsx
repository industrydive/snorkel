import cx from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';

import '../scss/components/HelpText.scss';

const HelpText = (props) => {
  const {
    children,
    size,
  } = props;

  const helpTextClasses = cx({
    'help-text': true,
    'help-text--small': size === 'small',
  });

  return (
    <p className={helpTextClasses}>
      {children}
    </p>
  );
};

HelpText.propTypes = {
  children: PropTypes.node.isRequired,
  size: PropTypes.oneOf(['small', 'large']),
};

HelpText.defaultProps = {
  size: 'large',
};

export default HelpText;
