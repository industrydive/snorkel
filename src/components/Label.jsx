import cx from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';

import '../scss/components/Label.scss';

const LabelIcon = (props) => {
  const {
    alt,
    src,
  } = props;

  return (
    <img src={src} alt={alt} className="label__icon" />
  );
};

LabelIcon.propTypes = {
  alt: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
};

const Label = (props) => {
  const {
    children,
    secondary,
    type,
  } = props;

  const labelClasses = cx({
    label: !secondary,
    'secondary-label': secondary,
    'label--loud': !secondary && type === 'loud',
    'label--medium': !secondary && type === 'medium',
    'label--soft': !secondary && type === 'soft',
    'label--sponsored': !secondary && type === 'sponsored',
    'secondary-label--loud': secondary && type === 'loud',
  });

  return (
    <span className={labelClasses}>
      {children}
    </span>
  );
};

Label.propTypes = {
  children: PropTypes.node.isRequired,
  secondary: PropTypes.bool,
  type: ((props) => {
    if (props.secondary === true) {
      return PropTypes.oneOf('loud', 'regular');
    }
    return PropTypes.oneOf(['loud', 'medium', 'soft', 'sponsored']);
  }).isRequired,
};

Label.defaultProps = {
  secondary: false,
};

Label.Icon = LabelIcon;

export default Label;
