const path = require('path');
const fsPromises = require('fs').promises;
// eslint-disable-next-line import/no-extraneous-dependencies
const { pascalCase } = require('change-case');
// eslint-disable-next-line import/no-extraneous-dependencies
const Mustache = require('mustache');

const createFile = async (filePath, templatePath, componentName) => {
  const template = await fsPromises.readFile(templatePath, 'utf-8');
  const fileContents = Mustache.render(template, { componentName });
  return fsPromises.writeFile(filePath, fileContents);
};

const createScssFile = async (componentName) => {
  const filePath = path.join(__dirname, '..', 'src', 'scss', 'components', `${componentName}.scss`);
  const templatePath = path.join(__dirname, 'templates', 'scssFile.mustache');
  await createFile(filePath, templatePath, componentName);
  // eslint-disable-next-line no-console
  console.log(`Created ${filePath}!`);
};

const createStoryFile = async (componentName) => {
  const filePath = path.join(__dirname, '..', 'src', 'components', `${componentName}.jsx`);
  const templatePath = path.join(__dirname, 'templates', 'componentFile.mustache');
  await createFile(filePath, templatePath, componentName);
  // eslint-disable-next-line no-console
  console.log(`Created ${filePath}!`);
};

const createComponentFile = async (componentName) => {
  const filePath = path.join(__dirname, '..', 'stories', `${componentName}.stories.jsx`);
  const templatePath = path.join(__dirname, 'templates', 'storyFile.mustache');
  await createFile(filePath, templatePath, componentName);
  // eslint-disable-next-line no-console
  console.log(`Created ${filePath}!`);
};

const args = process.argv.slice(2);

const componentName = pascalCase(args[0]);

try {
  createScssFile(componentName);
  createStoryFile(componentName);
  createComponentFile(componentName);
} catch (error) {
  // eslint-disable-next-line no-console
  console.log(error);
}
